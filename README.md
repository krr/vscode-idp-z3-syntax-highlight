# idpz3 README

(IDP-Z3)[https://www.idp-z3.be/] is a FO(.) language suited for creating knowledge bases.

## Features

This extension provides a syntax highlighting for IDP-Z3 language.

## Known Issues

- Still does not support the entire syntax!

## Release Notes

### 0.0.1

The firs (beta) version.

## Build the package 

Install the `vsce` using the following command: `npm install -g vsce`
Use the command `vsce package` to make the package. 


